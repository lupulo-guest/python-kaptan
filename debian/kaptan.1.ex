.\" Man page generated from reStructuredText.
.
.TH "KAPTAN" "1" "Apr 22, 2020" "0." "kaptan"
.SH NAME
kaptan \- kaptan Documentation
.
.nr rst2man-indent-level 0
.
.de1 rstReportMargin
\\$1 \\n[an-margin]
level \\n[rst2man-indent-level]
level margin: \\n[rst2man-indent\\n[rst2man-indent-level]]
-
\\n[rst2man-indent0]
\\n[rst2man-indent1]
\\n[rst2man-indent2]
..
.de1 INDENT
.\" .rstReportMargin pre:
. RS \\$1
. nr rst2man-indent\\n[rst2man-indent-level] \\n[an-margin]
. nr rst2man-indent-level +1
.\" .rstReportMargin post:
..
.de UNINDENT
. RE
.\" indent \\n[an-margin]
.\" old: \\n[rst2man-indent\\n[rst2man-indent-level]]
.nr rst2man-indent-level -1
.\" new: \\n[rst2man-indent\\n[rst2man-indent-level]]
.in \\n[rst2man-indent\\n[rst2man-indent-level]]u
..
.sp
\fI\%Python Package\fP \fI\%Documentation Status\fP \fI\%Build Status\fP \fI\%Code Coverage\fP [image: License]
[image]

.sp
configuration parser.
.SH INSTALLATION
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
$ pip install kaptan
.ft P
.fi
.UNINDENT
.UNINDENT
.sp
Also available as a package on FreeBSD, Debian, Arch Linux and Slackware.
.SH USAGE
.sp
\fBsupported handlers\fP
.INDENT 0.0
.IP \(bu 2
dict
.IP \(bu 2
json
.IP \(bu 2
yaml
.IP \(bu 2
\&.ini
.IP \(bu 2
python file
.UNINDENT
.sp
\fBdefault (dict) handler\fP
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
config = kaptan.Kaptan()
config.import_config({
    \(aqenvironment\(aq: \(aqDEV\(aq,
    \(aqredis_uri\(aq: \(aqredis://localhost:6379/0\(aq,
    \(aqdebug\(aq: False,
    \(aqpagination\(aq: {
        \(aqper_page\(aq: 10,
        \(aqlimit\(aq: 20,
    }
})

print config.get("pagination.limit")

# output: 20
.ft P
.fi
.UNINDENT
.UNINDENT
.sp
\fBjson handler\fP
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
config = kaptan.Kaptan(handler="json")
config.import_config(\(aq{"everything": 42}\(aq)

print config.get("everything")
# output: 42
.ft P
.fi
.UNINDENT
.UNINDENT
.sp
\fByaml handler\fP
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
config = kaptan.Kaptan(handler="yaml")
config.import_config("""
product:
  price:
    value: 12.65
    currency_list:
      1. TL
      2. EURO
""")
print config.get("product.price.currency_list.0")
# output: TL
.ft P
.fi
.UNINDENT
.UNINDENT
.sp
or you can get from directly from the filename:
.sp
\fBconfig.import_config("configuration.yaml")\fP
.sp
\fB\&.ini handler\fP
.sp
config.ini
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
[development]
database_uri = mysql://root:123456@localhost/posts

[production]
database_uri = mysql://poor_user:poor_password@localhost/poor_posts
.ft P
.fi
.UNINDENT
.UNINDENT
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
config = kaptan.Kaptan(handler="ini")
config.import_config(\(aqconfig.ini\(aq)

print config.get("production.database_uri")
# output: mysql://poor_user:poor_password@localhost/poor_posts
.ft P
.fi
.UNINDENT
.UNINDENT
.sp
\fBfile handler\fP
.sp
config.py
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
DATABASE = \(aqmysql://root:123456@localhost/posts\(aq
DEBUG = False
PAGINATION = {
    \(aqper_page\(aq: 10,
    \(aqlimit\(aq: 20,
}
.ft P
.fi
.UNINDENT
.UNINDENT
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
config = kaptan.Kaptan(handler="file")
config.import_config(\(aqconfig\(aq)

print config.get("DEBUG")
# output: False
.ft P
.fi
.UNINDENT
.UNINDENT
.SH EXPORTING CONFIGURATION
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
config = kaptan.Kaptan(handler="file")
config.import_config({
    \(aqenvironment\(aq: \(aqDEV\(aq,
    \(aqredis_uri\(aq: \(aqredis://localhost:6379/0\(aq,
    \(aqdebug\(aq: False,
    \(aqpagination\(aq: {
        \(aqper_page\(aq: 10,
        \(aqlimit\(aq: 20,
    }
})

print config.export("yaml")
.ft P
.fi
.UNINDENT
.UNINDENT
.sp
\fBoutput\fP:
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
debug: false
environment: DEV
pagination: {limit: 20, per_page: 10}
redis_uri: redis://localhost:6379/0
.ft P
.fi
.UNINDENT
.UNINDENT
.sp
\fBprint config.export("json")\fP
.sp
outputs unindented json. \fB\&.export\fP accepts kwargs which pass into
\fIjson.dumps\fP\&.
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
print config.export("json", indent=4)
.ft P
.fi
.UNINDENT
.UNINDENT
.sp
\fBoutput\fP:
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
{
    "environment": "DEV",
    "debug": false,
    "pagination": {
        "per_page": 10,
        "limit": 20
    },
    "redis_uri": "redis://localhost:6379/0"
}
.ft P
.fi
.UNINDENT
.UNINDENT
.sp
\fBconfig.export(\(aqyaml\(aq)\fP also supports the \fI\%kwargs for pyyaml\fP\&.
.sp
New in Version 0.5.7: \fBconfig.export(\(aqyaml\(aq, safe=True)\fP will use \fB\&.safe_dump\fP\&.
.SH CLI
.sp
exporting (defaults to json)
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
$ echo "environment: DEV" > config.yaml
$ kaptan config.yaml \-\-export json > config.json
$ cat config.json
{"environment": "DEV"}
.ft P
.fi
.UNINDENT
.UNINDENT
.sp
getting a value
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
$ kaptan config.yaml \-\-key environment
DEV
.ft P
.fi
.UNINDENT
.UNINDENT
.sp
specifying the handler
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
$ mv config.yaml config.settings
$ kaptan config.settings:yaml \-\-export json
{"environment": "DEV"}
.ft P
.fi
.UNINDENT
.UNINDENT
.sp
config from stdin
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
$ echo \(aq{"source": "stdin"}\(aq | kaptan \-
{"source": "stdin"}
$ echo \(aqsource: stdin\(aq | kaptan \-:yaml
{"source": "stdin"}
.ft P
.fi
.UNINDENT
.UNINDENT
.sp
merging configs
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
$ echo "environment: PROD" > config.settings
$ echo \(aq{"source": "stdin"}\(aq | kaptan \- config.json config.settings:yaml
{"environment": "PROD", "source": "stdin"}
.ft P
.fi
.UNINDENT
.UNINDENT
.sp
setting default handler
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
$ echo "source: stdin" | kaptan \-\-handler yaml \- config.settings
{"environment": "PROD", "source": "stdin"}
.ft P
.fi
.UNINDENT
.UNINDENT
.sp
writing json with yaml
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
$ kaptan \-:yaml \-e json
<type yaml here>
<Ctrl + D>
<get json here>
.ft P
.fi
.UNINDENT
.UNINDENT
.SH RUNNING TESTS
.sp
with \fBpy.test\fP:
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
$ py.test
.ft P
.fi
.UNINDENT
.UNINDENT
.SH CONTRIBUTORS
.INDENT 0.0
.IP \(bu 2
\fI\%Cenk Altı\fP
.IP \(bu 2
\fI\%Wesley Bitter\fP
.IP \(bu 2
\fI\%Mark Steve\fP
.IP \(bu 2
\fI\%Tony Narlock\fP
.IP \(bu 2
\fI\%Berker Peksag\fP
.IP \(bu 2
\fI\%Pradyun S. Gedam\fP
.UNINDENT
.sp
see more at \fI\%https://github.com/emre/kaptan/graphs/contributors\fP\&.
.sp
Explore:
.SS API Reference
.SS kaptan
.sp
configuration parser.
.INDENT 0.0
.TP
.B copyright
.INDENT 7.0
.IP c. 3
2013 by the authors and contributors (See AUTHORS file).
.UNINDENT
.TP
.B license
BSD, see LICENSE for more details.
.UNINDENT
.INDENT 0.0
.TP
.B class kaptan.Kaptan(handler=None)
Bases: \fI\%object\fP
.INDENT 7.0
.TP
.B HANDLER_MAP = {\(aqdict\(aq: <class \(aqkaptan.handlers.dict_handler.DictHandler\(aq>, \(aqfile\(aq: <class \(aqkaptan.handlers.pyfile_handler.PyFileHandler\(aq>, \(aqini\(aq: <class \(aqkaptan.handlers.ini_handler.IniHandler\(aq>, \(aqjson\(aq: <class \(aqkaptan.handlers.json_handler.JsonHandler\(aq>, \(aqyaml\(aq: <class \(aqkaptan.handlers.yaml_handler.YamlHandler\(aq>}
.UNINDENT
.INDENT 7.0
.TP
.B upsert(key, value)
.UNINDENT
.INDENT 7.0
.TP
.B _is_python_file(value)
Return True if the \fIvalue\fP is the path to an existing file with a
\fI\&.py\fP extension. False otherwise
.UNINDENT
.INDENT 7.0
.TP
.B import_config(value)
.UNINDENT
.INDENT 7.0
.TP
.B _get(key)
.UNINDENT
.INDENT 7.0
.TP
.B get(key=None, default=<object object>)
.UNINDENT
.INDENT 7.0
.TP
.B export(handler=None, **kwargs)
.UNINDENT
.INDENT 7.0
.TP
.B _Kaptan__handle_default_value(key, default)
.UNINDENT
.UNINDENT
.INDENT 0.0
.TP
.B kaptan.get_parser()
Create and return argument parser.
.INDENT 7.0
.TP
.B Return type
\fI\%argparse.ArgumentParser\fP
.TP
.B Returns
CLI Parser
.UNINDENT
.UNINDENT
.INDENT 0.0
.TP
.B kaptan.main()
.UNINDENT
.INDENT 0.0
.TP
.B class kaptan.Kaptan(handler=None)
Bases: \fI\%object\fP
.INDENT 7.0
.TP
.B _is_python_file(value)
Return True if the \fIvalue\fP is the path to an existing file with a
\fI\&.py\fP extension. False otherwise
.UNINDENT
.UNINDENT
.SS Handlers
.INDENT 0.0
.TP
.B class kaptan.handlers.BaseHandler
Bases: \fI\%object\fP
.sp
Base class for data handlers.
.INDENT 7.0
.TP
.B dump(data)
.UNINDENT
.INDENT 7.0
.TP
.B load(data)
.UNINDENT
.UNINDENT
.INDENT 0.0
.TP
.B class kaptan.handlers.dict_handler.DictHandler
Bases: \fI\%kaptan.handlers.BaseHandler\fP
.INDENT 7.0
.TP
.B dump(data)
.UNINDENT
.INDENT 7.0
.TP
.B load(data)
.UNINDENT
.UNINDENT
.INDENT 0.0
.TP
.B class kaptan.handlers.ini_handler.IniHandler
Bases: \fI\%kaptan.handlers.BaseHandler\fP
.INDENT 7.0
.TP
.B dump(data, file_=None)
.UNINDENT
.INDENT 7.0
.TP
.B load(value)
.UNINDENT
.UNINDENT
.INDENT 0.0
.TP
.B class kaptan.handlers.json_handler.JsonHandler
Bases: \fI\%kaptan.handlers.BaseHandler\fP
.INDENT 7.0
.TP
.B dump(data, **kwargs)
.UNINDENT
.INDENT 7.0
.TP
.B load(data)
.UNINDENT
.UNINDENT
.INDENT 0.0
.TP
.B class kaptan.handlers.yaml_handler.YamlHandler
Bases: \fI\%kaptan.handlers.BaseHandler\fP
.INDENT 7.0
.TP
.B dump(data, safe=True, **kwargs)
.UNINDENT
.INDENT 7.0
.TP
.B load(data, safe=True)
.UNINDENT
.UNINDENT
.SS Command Line Interface
.sp
Configuration manager in your pocket

.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
usage: kaptan [\-h] [\-\-handler HANDLER] [\-e EXPORT] [\-k KEY]
              [config_file [config_file ...]]
.ft P
.fi
.UNINDENT
.UNINDENT
.SS Positional Arguments
.INDENT 0.0
.TP
.Bconfig_file
file/s to load config from
.UNINDENT
.SS Named Arguments
.INDENT 0.0
.TP
.B\-\-handler
set default handler
.sp
Default: "json"
.TP
.B\-e, \-\-export
set format to export to
.sp
Default: "json"
.TP
.B\-k, \-\-key
set config key to get value of
.UNINDENT
.SS History
.sp
Here you can find the recent changes to kaptan
.SS v0.5.12 (2019\-04\-22)
.INDENT 0.0
.IP \(bu 2
Bump pipenv version to python 3.x
.IP \(bu 2
Utility package updates
.IP \(bu 2
Bump pyyaml version to <6
.UNINDENT
.SS v0.5.11 (2018\-12\-30)
.INDENT 0.0
.IP \(bu 2
Update dependencies for pytest, sphinx, etc.
.IP \(bu 2
Forward compatiblity with Python 3.8 \fBcollections.abc\fP
.IP \(bu 2
sphinx\-argparse 0.2.2 to 0.2.5
.IP \(bu 2
Sphinx 1.7.5 to 1.8.3
.IP \(bu 2
sphinx\-rtd\-theme 0.4.0 to 0.4.2
.IP \(bu 2
flake8 3.5.0 to 3.6.0
.IP \(bu 2
pytest 3.6.2 to 4.0.2
.UNINDENT
.SS v0.5.10 (2018\-07\-06)
.INDENT 0.0
.IP \(bu 2
Update Pipfile
.IP \(bu 2
Relax pyyaml requirements to <4
.IP \(bu 2
Support for Python 3.7
.IP \(bu 2
Prevent test from creating stray .pyc files
.IP \(bu 2
Update pytest 3.2.3 to 3.6.3
.IP \(bu 2
Update sphinx 1.6.4 to 1.7.5
.IP \(bu 2
Update sphinx\-argparse 0.2.1 to 0.2.2
.IP \(bu 2
Update sphinx\-rtd\-theme 0.2.4 to 0.4.0
.IP \(bu 2
Add \fBmake sync_pipfile\fP for updating Pipfile
.UNINDENT
.SS v0.5.9 (2017\-10\-19)
.INDENT 0.0
.IP \(bu 2
Make yaml safe by default (\fI\%https://github.com/emre/kaptan/pull/46\fP) by
@pradyunsg
.IP \(bu 2
Add \fBPipfile\fP for \fI\%pipenv\fP
.IP \(bu 2
Add \fB\&.tmuxp.yaml\fP for \fI\%tmuxp\fP
.IP \(bu 2
Add \fBMakefile\fP with tasks to re\-run commands on file save, rebuild
sphinx
.IP \(bu 2
Update py.test, sphinx versions
.UNINDENT
.SS Roadmap
.sp
See \fI\%https://github.com/emre/kaptan/issues\fP\&.
.SH AUTHOR
Emre Yilmaz
.SH COPYRIGHT
Copyright 2013-2019 Emre Yilmaz
.\" Generated by docutils manpage writer.
.
